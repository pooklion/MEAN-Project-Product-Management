const express = require('express');
let app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/ppm');
mongoose.Promise=global.Promise;
app.use(bodyParser.json());
let path = require('path');
app.use(express.static(path.join(__dirname, 'ppmApp', 'dist')))


let PPMSchema = new mongoose.Schema({
    title : {type: String, required:true, minlength: 4},
    price : {type: Number, required: true},
    img : {type : String}
}, {timestamps: true });

mongoose.model('PPM', PPMSchema);
let PPM = mongoose.model('PPM');

// create
app.post('/product', (req,res) =>{
    // console.log ('server',req.body)
    PPM.create(req.body,(err) =>{
        if(err){
            res.json(err);
        }else{
            res.status(200).json({message: 'Product Created!'})
        }
    })
})
// show all
app.get('/products', (req,res) =>{
    console.log('server Get')
    PPM.find({}).sort('title').exec((err, data) => {
        console.log(data)
        if (err){
            res.json({message: 'Error!', error:err})
        }else{
            res.json({message: 'Found all products!', data:data})
        }
    })
})
// show one
app.get('/product/:id', (req,res) =>{
    console.log('server',req.params.id)
    PPM.findOne({_id:req.params.id}, (err, data) => {
        console.log(data);
        if (err){
            res.json({message: 'Error!', error:err})
        }else{
            res.json({message: 'Success!', data:data})
        }
    })
})
// edit
app.put('/product/:id',(req,res) => {
    PPM.update({_id:req.params.id}, req.body, { runValidators: true }, (err,data) => {
        if (err){
            res.json(err);
        }else{
            res.json({message: 'Success!', result:data})
        }
    })
})
// delete
app.delete('/product/:_id',(req,res) => {
    console.log('dlklkl')
    PPM.remove({_id:req.params._id}, (err,data) => {
        if (err){
            res.json({message:'Error!', message:err})
        }else{
            res.json({message:'Deleted!!'})
        }
    })
})

app.all("*", (req,res,next) => {
    res.sendFile(path.join(__dirname, 'ppmApp','dist','index.html'))
});

app.listen(8000, ()=> {
    console.log('on 8000');
}) 