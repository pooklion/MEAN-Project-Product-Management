import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  errors:string[];
  product:any;
  constructor(
    private _httpService:HttpService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.product = {title:'',price:'',img:''};
  }

  createProduct(){
    let obs = this._httpService.serviceCreate(this.product);
    obs.subscribe ((res:any) => {
      this.errors = [];
      if (res.errors){
        for (let key in res.errors) {
          this.errors.push(res.errors[key].message)
        }
      }else{
        this.goHome();
      }
    })
  }

  goHome(){
    this._router.navigate(['/product']);
  }

}
