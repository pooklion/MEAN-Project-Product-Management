import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products=[];
  constructor(
    private _httpService:HttpService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    console.log('getting!!')
    let obs = this._httpService.serviceGetAll();
    obs.subscribe(res =>{
      console.log('res:',res)
      this.products = res['data'];
    })
  }
  delete(id){
    let obs = this._httpService.serviceDelete(id);
    obs.subscribe(res => {
      console.log('deleted: ', res)
    })
    this.getAll();
  }

  goHome(){
    this._router.navigate(['/']);
  }

}
