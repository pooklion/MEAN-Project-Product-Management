import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {

  constructor(private _http: HttpClient) { }

  serviceCreate(productData){
    console.log('service: ',productData)
    return this._http.post('/product', productData);
  }
  serviceGetAll(){
    console.log('service: ')
    return this._http.get('/products');
  }
  serviceDelete(id){
    return this._http.delete('/product/'+id);
  }
  serviceGetOne(id){
    return this._http.get('/product/'+id);
  }
  serviceEdit(id, newData){
    console.log('service: ',id,newData)
    return this._http.put('/product/'+id, newData);
  }
}
