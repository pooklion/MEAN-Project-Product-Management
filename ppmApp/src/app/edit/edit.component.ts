import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  product ={title:'',price:'',img:''};
  id:any;
  errors: string[];

  constructor(
    private _httpService: HttpService,
    private _router: Router,
    private route: ActivatedRoute) { 
      this.route.params.subscribe(params => {
        this.id = params.id;
      })
    }

  ngOnInit() {
    this.getOne(this.id);

  }
  getOne(id){
    console.log ('id: ',id)
    let obs = this._httpService.serviceGetOne(id)
    obs.subscribe((res:any) => {
      console.log('res:', res)
      // this.author = data.data;
      this.product = res.data;
    })

  }
  editProduct(id){
    let obs = this._httpService.serviceEdit(id,this.product)
    obs.subscribe((data:any) => {
      this.errors = [];
      if (data.errors){
        for (let key in data.errors){
          this.errors.push(data.errors[key].message);
        }     
      } else {
        console.log('Edited!!!' , data);
        this._router.navigate(['/product']);
      }
    })
  }
}
